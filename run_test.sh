#!/bin/bash

# --- checking format
#[ $(clang-format -style="{BasedOnStyle: Mozilla, IndentWidth: 8}" -i *.cpp -output-replacements-xml |  grep -c "replacement " | tr -d "[:cntrl:]") -gt 0 ] && echo "Format is INVALID" && exit 1 || echo "Format is VALID"
[ $(clang-format -style="{BasedOnStyle: llvm, IndentWidth: 8}" -i *.cpp -output-replacements-xml |  grep -c "replacement " | tr -d "[:cntrl:]") -gt 0 ] && echo "Format is INVALID" && exit 1 || echo "Format is VALID"
# --- checking format

# --- cmake && make
mkdir -p build && cd build && cmake .. && make
# --- cmake && make
